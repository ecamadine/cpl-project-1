/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 1st Deliverable

	SCL_Helpers.h
	
	Purpose: Defines the helper functions.
*/

namespace SCL_Helpers
{
	bool IsAlphabetic(char Value);
	bool IsAlphabeticOrNumeric(char Value);
	bool IsDigit(char Value);
	bool IsHexadecimalDigit(char Value);
	bool IsOctalDigit(char Value);

	unsigned int HashString(const char *String);

	void Log(const char* Message, ...);
}