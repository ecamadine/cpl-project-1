/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 1st Deliverable

	SCL_Keywords.h
	
	Purpose: Defines the struct definition used to define the keywords of the SCL.
*/

typedef struct
{
		const char* KeywordName;
		unsigned int Code;
}Keyword_t;

namespace SCL_Keywords
{
	extern Keyword_t KeywordArray[];
	
	extern size_t SizeOfKeywordArray;
}