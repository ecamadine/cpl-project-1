/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 1st Deliverable

	SCL_Helpers.cpp
	
	Purpose: Implements the helper functions.
*/

#include "RequiredHeaders.h"

using namespace std;

namespace SCL_Helpers
{
	bool IsAlphabetic(char Value)
	{
		return ( Value >= 'a' && Value <= 'z' || Value >= 'A' && Value <= 'Z' ) ? true : false;
	}

	bool IsAlphabeticOrNumeric(char Value)
	{
		return ( Value >= '0' && Value <= '9' || Value >= 'a' && Value <= 'z' || Value >= 'A' && Value <= 'Z' ) ? true : false;
	}

	bool IsDigit(char Value)
	{
		return ( Value >= '0' && Value <= '9' ) ? true : false;
	}

	bool IsHexadecimalDigit(char Value)
	{
		return (Value >= '0' && Value <= '9' || Value >= 'a' && Value <= 'f' || Value >= 'A' && Value <= 'F' ) ? true : false;
	}

	bool IsOctalDigit(char Value)
	{
		return (Value >= '0' && Value <= '7');
	}
	
	unsigned int HashString(const char *String)
	{
		unsigned int Hash = 5381;
	
		int Length = (int)strlen(String);
	
		for ( int Iterator = 0; Iterator < Length; ++Iterator )
		{
			Hash = (Hash * 33) ^ String[Iterator];
		}
	
		return Hash;
	}

	void Log (const char* Message, ...)
	{		
		int Number = 0;

		char buffer[MAX_BUFFER_SIZE + 1];
		va_list args;
		va_start(args, Message);
		vsnprintf(buffer, MAX_BUFFER_SIZE, Message, args);
		
		string ErrorMessage = buffer;

		ofstream File;
		File.open("log.txt", ios::app);
		File << ErrorMessage << endl;
		File.close();

		va_end(args);
	}

	ofstream &operator << (std::ofstream &FileObject, string ErrorMessage )
	{		
		FileObject << ErrorMessage << endl;

		return FileObject;
	}
}