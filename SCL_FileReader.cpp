/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 1st Deliverable

	SCL_FileReader.cpp
	
	Purpose: Implements the methods of the class SCL_FileReader.
*/

#include "RequiredHeaders.h"

using namespace std;

#include "SCL_FileReader.h"

SCL_FileReader::SCL_FileReader(string FileName, int Flags)
{	
	File.open(FileName, Flags);

	if (File.is_open())
	{
		FileBegin = File.tellg();
	  	File.seekg (0, ios::end);
		FileEnd = File.tellg();
				
		FileSize = FileEnd - FileSize;
		
		File.seekg (0, ios::beg);

		this->FileName = FileName;
	}
	else
	{
		cout << "Issue in FileReader constructor." << endl;
	}
}

SCL_FileReader::~SCL_FileReader()
{
	if (IsFileOpen())
	{
		CloseFile();
	}
}

streampos SCL_FileReader::GetFileSize()
{	
	if (IsFileOpen())
	{
		return FileSize;
	}
	
	return -1;
}
		
bool SCL_FileReader::CloseFile()
{
	return (IsFileOpen()) ? File.close(), true  : false;
}

bool SCL_FileReader::OpenFile(string FileName, int Flags)
{
	bool ReturnValue = true;
	
	if (IsFileOpen())
	{
		if (CloseFile() == false)
		{
			cout << "Failed to close existing opened file in OpenFile." << endl;
			
			ReturnValue = false;
		}
	}
	else
	{
		this->FileName = FileName;

		File.open(FileName, Flags);
			
		if (IsFileOpen() == false)
		{
			cout << "Failed to open file in OpenFile." << endl;
				
			ReturnValue = false;
		}
	}
	
	return ReturnValue;
}

bool SCL_FileReader::ReadLine(string& Line)
{
	bool ReturnValue = true;
	
	if (!getline (File, Line))
	{
		ReturnValue = false;
	}
	return ReturnValue;
}

bool SCL_FileReader::ReadLine(string& Line, char Delim)
{
	bool ReturnValue = true;
	
	if (!getline (File, Line, Delim))
	{
		ReturnValue = false;
	}
	
	return ReturnValue;
}

bool SCL_FileReader::IsFileOpen()
{
	return File.is_open();
}