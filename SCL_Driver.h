/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 1st Deliverable

	SCL_Driver.h
	
	Purpose: Defines the class that drives the SCL scanner.
*/

class SCL_Driver 
{
	public:
	
		SCL_Driver();
		~SCL_Driver();
		
		void Initialize(char* SCL_FileName);
		
	private:
		string FileName;	
};