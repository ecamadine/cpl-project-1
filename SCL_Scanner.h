/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 1st Deliverable

	SCL_Scanner.h
	
	Purpose: Defines the class that handles the lexical analysis phase.
*/

enum
{
	Normal = 0,
	Error,
};



typedef struct
{
	bool usekeywordtable;
	int token;
}Token_t;

typedef struct
{
	string lexeme;
}Lexeme_t;

typedef struct
{
	Token_t TokenEntry;

	Lexeme_t LexemeEntry;

	unsigned int Line;
}Lexemes_and_Tokens_t;

extern const char* TokenNames[];

class SCL_Scanner : public CommonComponentInterface
{
	public:
	
		SCL_Scanner(SCL_FileReader* Reader);
		~SCL_Scanner();
				
		void ClassInitializationRoutine();							
		void Start();
		void Finish();
		
		unsigned int ScanLine(string& Line);

		void AddEntry(Lexemes_and_Tokens_t &entry);

		void AdvanceCharacter(string& Line)
		{
			if ((CurrentCharacterIndex + 1) < Line.length())
			{
				LastCharacterIndex = CurrentCharacterIndex++;
			}
		}

		bool GetCharacterFromLine(string& Line, char& CharacterOut);
		char GetCurrentCharacter();
		char GetNextCharacter();

		void SetState(unsigned int NewState)
		{
			State = NewState;
		}

		unsigned int GetState()
		{
			return State;
		}

		void PreviousCharacter(string& Line)
		{
			if ((CurrentCharacterIndex - 1) > 0)
			{
				LastCharacterIndex = --CurrentCharacterIndex;
			}
		}

		void RestoreCharacterIndicies()
		{
			if (Stored == true)
			{
				Stored = false;
			
				CurrentCharacterIndex = StoredCurrentCharacterIndex;
				LastCharacterIndex = StoredLastCharacterIndex;
			}
		}

		void StoreCharacterIndicies()
		{
			if (Stored != true)
			{
				Stored = true;

				StoredCurrentCharacterIndex = CurrentCharacterIndex;
				StoredLastCharacterIndex = LastCharacterIndex;
			}
		}

	protected:
	
		
	private:

		bool InMultiLineComment;

		bool AtLineEnd(string& Line);

		bool Stored;
		
		char CurrentCharacter;
		char NextCharacter;

		unsigned int CheckForNonSymbol(string& Line, char Character);

		string Comment;
		string Symbol;
		string PreviousSymbol;
		
		SCL_FileReader* FileReader;
		
		map<unsigned int, unsigned int> MapOfKeywords;
		
		Lexemes_and_Tokens_t* GetLexemesAndTokensByEntry(unsigned int EntryIntoVector);

		vector<Lexemes_and_Tokens_t>& GetLexemesAndTokenVector();
	
		unsigned int State;
		
		void StartLexicalAnalysis();
		void FinishLexicalAnalysis();
		
		size_t CurrentCharacterIndex;
		size_t LastCharacterIndex;
		size_t StoredCurrentCharacterIndex;
		size_t StoredLastCharacterIndex;
		
		size_t LineNumber;

		vector<Lexemes_and_Tokens_t> LexemesAndTokensVector;
};