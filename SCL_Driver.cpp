/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 1st Deliverable

	SCL_Driver.cpp
	
	Purpose: Implements the methods of the class SCL_Driver.
*/

#include "RequiredHeaders.h"

using namespace std;

#include "CommonComponentInterface.h"
#include "SCL_FileReader.h"
#include "SCL_Driver.h"
#include "SCL_Helpers.h"
#include "SCL_Scanner.h"

SCL_Driver::SCL_Driver()
{
	FileName = "";
}

SCL_Driver::~SCL_Driver()
{
	
}

void SCL_Driver::Initialize(char* SCL_FileName)
{
	FileName = SCL_FileName;
	
	SCL_Scanner* Scanner = nullptr; // lexical
	
	SCL_FileReader* Reader = nullptr;
	
	Reader = new SCL_FileReader(FileName, ios::in);
				
	if (Reader != nullptr)
	{							
		if (Reader->IsFileOpen())
		{
			Scanner = new SCL_Scanner(Reader); 
										
			if (Scanner == nullptr)
			{
				cout << "Memory allocation failed." << endl;
			}
			else
			{
				Scanner->ClassInitializationRoutine();

				Scanner->Start();
				Scanner->Finish();
			}
				
			if (Reader->CloseFile() == false)
			{
				cout << "Failed to close file " << FileName << " ." << endl;
			}
		}
		else
		{
			cout << "Specified file does not exist." << endl;

			SCL_Helpers::Log("Specified file %s does not exist.", FileName.c_str());
		}
	}
		
	if (Reader != nullptr)
	{
		delete Reader;
	}
	if (Scanner != nullptr)
	{
		delete Scanner;
	}
}