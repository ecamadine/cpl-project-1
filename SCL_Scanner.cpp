/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 1st Deliverable

	SCL_Scanner.h
	
	Purpose: Implements the class methods that handles the lexical analysis phase.
*/

#include "RequiredHeaders.h"

using namespace std;

#include "SCL_FileReader.h"

#include "CommonComponentInterface.h"
#include "SCL_Helpers.h"
#include "SCL_Driver.h"
#include "SCL_Keywords.h"
#include "SCL_Scanner.h"
#include "SCL_Symbols.h"

const char* TokenNames[] = // this feels gross, should we do this some place else?
{	
	"reserved",
	"default", //
	"and_operator", //
	"assignment_operator", //
	"block_begin", // {
	"block_end", // }
	"character_literal", //
	"conditional_if_operator", // ? : but not really implemented!
	"delimiter", // comma
	"comment_multiline", // /* */
	"comment_singleline", //
	"division_operator", //
	"endoffile", //
	"equality", //
	"exponent_operator", //
	"float_literal", //
	"hexadecimal_literal", //
	"identifier", //
	"integer_literal", //
	"keyword", // redundant?
	"leftbracket", //
	"leftparentheses", //
	"linefeed", //
	"minus_operator", //
	"modulus_operator", //
	"multiplication_operator", // asterisk
	"not_operator", //
	"or_operator", //
	"object_field_access_operator_period", //
	"object_field_access_operator_pointer", //
	"plus_operator", //
	"relational", //
	"relational_equals", //
	"rightbracket", //
	"rightparentheses", //
	"scope_resolution_operator", // ::
	"statement_terminator", // semi-colon
	"string_literal" //
};

SCL_Scanner::SCL_Scanner(SCL_FileReader* Reader)
{
	FileReader = Reader;

	Stored = false;
}

SCL_Scanner::~SCL_Scanner()
{
	
}

vector<Lexemes_and_Tokens_t>& SCL_Scanner::GetLexemesAndTokenVector()
{
	return LexemesAndTokensVector;
}

Lexemes_and_Tokens_t* SCL_Scanner::GetLexemesAndTokensByEntry(unsigned int EntryIntoVector)
{
	if (EntryIntoVector >= 0 && EntryIntoVector <= LexemesAndTokensVector.size()) // since unsigned int, is it needed? No!
	{
		for (unsigned int Iterator = 0; Iterator < EntryIntoVector; ++Iterator)
		{
			if (Iterator == EntryIntoVector)
			{
				return &LexemesAndTokensVector[Iterator];
			}
		}
	}

	return nullptr;
}

void SCL_Scanner::ClassInitializationRoutine()
{
	CurrentCharacterIndex = 0;
	LastCharacterIndex = 0;
	LineNumber = 0;

	// now intialize the map
	
	for (unsigned int Iterator = 0; Iterator < SCL_Keywords::SizeOfKeywordArray; ++Iterator)
	{
		MapOfKeywords[SCL_Helpers::HashString(SCL_Keywords::KeywordArray[Iterator].KeywordName)] = SCL_Keywords::KeywordArray[Iterator].Code;
	}
}

void SCL_Scanner::Start()
{
	StartLexicalAnalysis();
}

void SCL_Scanner::Finish()
{
	FinishLexicalAnalysis();
}

bool SCL_Scanner::AtLineEnd(string& Line)
{
	bool ReturnValue = false;

	if (CurrentCharacterIndex >= Line.length())
	{
		ReturnValue = true;
	}

	return ReturnValue;
}

char SCL_Scanner::GetCurrentCharacter()
{
	return CurrentCharacter;
}

char SCL_Scanner::GetNextCharacter()
{
	return NextCharacter;
}

bool SCL_Scanner::GetCharacterFromLine(string& Line, char& CharacterOut)
{	
	bool ReturnValue = true;

	size_t LineLength = Line.length();

	if (CurrentCharacterIndex < LineLength)
	{
		CurrentCharacter = Line[CurrentCharacterIndex];
		NextCharacter = Line[CurrentCharacterIndex + 1];
		
		if (CurrentCharacter == Character_Space || CurrentCharacter == Character_HorizontalTab)
		{
			while (CurrentCharacterIndex < LineLength && (CurrentCharacter == Character_Space || CurrentCharacter == Character_HorizontalTab))
			{
				LastCharacterIndex = CurrentCharacterIndex++;
				CurrentCharacter = Line[CurrentCharacterIndex];
			}

			CurrentCharacter = Line[LastCharacterIndex];
		}
		else
		{
			LastCharacterIndex = CurrentCharacterIndex++;
		}
	}
	else
	{
		ReturnValue = false;
	}

	CharacterOut = CurrentCharacter;

	return ReturnValue;
}

unsigned int SCL_Scanner::CheckForNonSymbol(string& Line, char Character)
{
	int LexicalReturnValue = Default;

	switch (Character)
	{
		case Character_Quotes:
		{
			Symbol += Character;
	
			while (GetCharacterFromLine(Line, Character) && Character != Character_Quotes)
			{
				if (Character == Character_NewLine) // C++ code does not suffer from this, but C code does, kept for that reason!
				{
					SCL_Helpers::Log("Missing quotes in string literal. \nLine Number %i, Current Character Count %i", LineNumber, CurrentCharacterIndex);

					cout << "Missing quotes in string literal." << endl;
					cout << "Line Number " << LineNumber << " Current Character Count "<< CurrentCharacterIndex << endl;

					SetState(Error);
				}
				else
				{
					Symbol += Character;
					
					if (Symbol.length() > MAX_BUFFER_SIZE)
					{
						SCL_Helpers::Log("String is too long.\nLine Number %i, Current Character Count %i", LineNumber, CurrentCharacterIndex);
						
						cout << "String is too long." << endl;
						cout << "Line Number " << LineNumber << " Current Character Count " << CurrentCharacterIndex << endl;

						SetState(Error);

						break;
					}
				}
			}
			if (Character == Character_NewLine)
			{
				Symbol += "\"";
			}
			else
			{
				if (Character != Character_Quotes)
				{
					SCL_Helpers::Log("Missing quotes in string literal. \nLine Number %i, Current Character Count %i", LineNumber, CurrentCharacterIndex);

					cout << "Missing quotes in string literal." << endl;
					cout << "Line Number " << LineNumber << " Current Character Count " << CurrentCharacterIndex << endl;

					SetState(Error);
				}
				else
				{
					Symbol += Character;
				}
			}
					
			LexicalReturnValue = String;
		}
		break;

		case Character_Apostrophe:
		{
//			char Buffer[MAX_BUFFER_SIZE];

			// process a char literal

			bool BaseCase = true;

			bool WasCharacterHexDefined = false;
			bool WasCharacterOctalDefined = false;
			bool WasCharacterUTFDefined = false;

			int Radix = 0;

			unsigned short CharacterCode = Character_NullCharacter;

			string Constant = "";

			Symbol += Character;
			
			while (GetNextCharacter() != Character_Apostrophe && GetCharacterFromLine(Line, Character))
			{
				if (Character == EscapeSequence_BackSlash)
				{
					if (GetNextCharacter() != 'u' && GetNextCharacter() != 'U'
						&& GetNextCharacter() != 'x' && GetNextCharacter() != 'X')
					{
						// check for common escape sequences first
						
						switch (GetNextCharacter())
						{
							case EscapeSequence_Apostrophe:
							case EscapeSequence_BackSpace:
							case EscapeSequence_BackSlash:
							case EscapeSequence_CarriageReturn:
							case EscapeSequence_EscapeCharacter:
							case EscapeSequence_FormFeed:
							case EscapeSequence_HorizontalTab:
							case EscapeSequence_NewLine:
							case EscapeSequence_Quotation:
							case EscapeSequence_QuestionMark:
							case EscapeSequence_VerticalTab:
							{
								Symbol += GetNextCharacter();
							}
							break;

							default:
							{
								// octal defined, or unknown escape sequence

								Radix = 8;

								BaseCase = false;

								while (GetCharacterFromLine(Line, Character) && SCL_Helpers::IsOctalDigit(Character))
								{
									Constant += Character;

									if (GetNextCharacter() == Character_Apostrophe)
									{
										break;
									}
								}

								if (Constant.length() != 0 && std::atoi(Constant.c_str()) != 0)
								{
									Symbol += std::stoi(Constant, nullptr, Radix);						
								}

							}
						}

						break; // get us out of the loop
					}
					else
					{
						if (GetCharacterFromLine(Line, Character))
						{
							Radix = 16;

							switch (Character)
							{
								case 'u':
								case 'U':
								{
									WasCharacterUTFDefined = true;
								}
								break;

								case 'x':
								case 'X':
								{
									WasCharacterHexDefined = true;
								}
								break;

							}

							BaseCase = false;

							while (GetCharacterFromLine(Line, Character) && SCL_Helpers::IsHexadecimalDigit(Character))
							{
								Constant += Character;

								if (GetNextCharacter() == Character_Apostrophe)
								{
									break;
								}
							}

							if (Constant.length() != 0 && std::atoi(Constant.c_str()) != 0)
							{
								Symbol += std::stoi(Constant, nullptr, Radix);
							}

							break; // get us out of the loop
						}
					}
				}
				else
				{
					Symbol += Character;

					break;
				}
			}

			if (GetCharacterFromLine(Line, Character) && Character != Character_Apostrophe)
			{
				Character = Character_Apostrophe;
				SCL_Helpers::Log("Missing closing apostrophe for character.\nLine Number %i, Current Character Count %i", LineNumber, CurrentCharacterIndex);

				cout << "Missing closing apostrophe for character." << endl;
				cout << "Line Number " << LineNumber << " Current Character Count " << CurrentCharacterIndex << endl;

				SetState(Error);
			}

			Symbol += Character;

			LexicalReturnValue = Letter;
		}
		break;
		
		case Character_Equals:
		{
			Symbol += Character;

			if (GetCharacterFromLine(Line, Character))
			{
				if (Character == '=')
				{
					Symbol += Character; // "=="

					LexicalReturnValue = Equality; // "=="
				}
				else
				{
					if (Character == '<' || Character == '>') // relational operator
					{
						Symbol += Character;

						LexicalReturnValue = Relational;
					}
					else // only one, so it is assignment
					{
						LexicalReturnValue = Assignment;
					}
				}
			}
		}
		break;

		case Character_LessThan:
		case Character_GreaterThan:
		{
			Symbol += Character;

			if (GetCharacterFromLine(Line, Character)) // <= or >=
			{
				if (Character == Character_Equals)
				{
					Symbol += Character;

					LexicalReturnValue = Relational_Equals;
				}
				else
				{
					LexicalReturnValue = Relational;
				}
			}
		}
		break;
		
		case Character_VerticalBar:
		{
			Symbol += Character;

			if (GetCharacterFromLine(Line, Character) && Character == Character_VerticalBar)
			{
				Symbol += Character;

				LexicalReturnValue = Or;
			}
			else
			{
				SCL_Helpers::Log("Error in conditional operator.\nLine Number %i, Current Character Count %i", LineNumber, CurrentCharacterIndex);

				cout << "Error in conditional operator." << endl;
				cout << "Line Number " << LineNumber << " Current Character Count " << CurrentCharacterIndex << endl;

				SetState(Error);
			}
		}
		break;
				
		case Character_Ampersand:
		{
			Symbol += Character;

			if (GetCharacterFromLine(Line, Character) && Character == Character_Ampersand)
			{
				Symbol += Character;

				LexicalReturnValue = And;
			}
			else
			{
				SCL_Helpers::Log("Error in conditional operator.\nLine Number %i, Current Character Count %i", LineNumber, CurrentCharacterIndex);

				cout << "Error in conditional operator." << endl;
				cout << "Line Number " << LineNumber << " Current Character Count " << CurrentCharacterIndex << endl;
				
				SetState(Error);
			}
		}
		break;

		case Character_ExclamationMark:
		{
			Symbol += Character;
			
			if (GetCharacterFromLine(Line, Character) && Character == '=')
			{
				Symbol += Character;

				LexicalReturnValue = Equality;
			}
			else // no =, so just plain !
			{
				LexicalReturnValue = Not;
			}
		}
		break;

		case Character_NewLine:
		{

		}
		break;

		case Character_CarriageReturn:
		{

		}
		break;

		case Character_LeftCurlyBrace:
		{
			LexicalReturnValue = LeftCurlyBrace;

			Symbol += Character;
		}
		break;

		case Character_RightCurlyBrace:
		{
			LexicalReturnValue = RightCurlyBrace;

			Symbol += Character;
		}
		break;

		case Character_Circumflex:
		{
			LexicalReturnValue = Exponent;

			Symbol += Character;
		}
		break;

		case Character_QuestionMark: // single line conditional
		case Character_Tilde:
		case Character_PoundSign:
		case Character_DollarSign:
		{
			SCL_Helpers::Log("Unrecognized character.\nLine Number %i, Current Character Count %i", LineNumber, CurrentCharacterIndex);
		
			cout << "Unrecognized character." << endl;
			cout << "Line Number " << LineNumber << " Current Character Count " << CurrentCharacterIndex << endl;
		}
		break;

		case Character_BackSlash:
		{
			LexicalReturnValue = Division; // temporary

			switch (GetNextCharacter())
			{
				case Character_BackSlash: // single line comment
				{
					PreviousSymbol = "";

					do
					{
						PreviousSymbol += GetCurrentCharacter();
					} while (GetCharacterFromLine(Line, Character));
				}
				break;

				case Character_Asterisk:
				{
					// continue advancing until we have consumed this line, or found the matching */

					InMultiLineComment = true;

					do
					{
						if (GetCurrentCharacter() == Character_Asterisk && GetNextCharacter() == Character_BackSlash) // found */ change state now
						{
							InMultiLineComment = false;

							break;
						}
					} while (GetCharacterFromLine(Line, Character));
				}
				break;
			}
		}
		break;

		case Character_Plus:
		{
			LexicalReturnValue = Plus;

			Symbol += Character;
		}
		break;

		case Character_Minus:
		{
			LexicalReturnValue = Minus;

			Symbol += Character;
		}
		break;

		case Character_Asterisk:
		{
			LexicalReturnValue = Asterisk;

			if (InMultiLineComment == true)
			{
				do
				{
					if (GetCurrentCharacter() == Character_Asterisk && GetNextCharacter() == Character_BackSlash) // found */ change state now
					{
						InMultiLineComment = false;

						break;
					}
				} while (GetCharacterFromLine(Line, Character));
			}
			else
			{
				Symbol += Character;
			}
		}
		break;

		case Character_Modulus:
		{
			LexicalReturnValue = Modulus;

			Symbol += Character;
		}
		break;

		case Character_Comma:
		{
			LexicalReturnValue = Comma;

			Symbol += Character;
		}
		break;

		case Character_Colon:
		{
			Symbol += Character;

			if (GetCharacterFromLine(Line, Character) && Character == ':')
			{
				Symbol += Character;

				LexicalReturnValue = DoubleColon;
			}
			else // no ::, so just plain :, ternary?
			{

			}

			LexicalReturnValue = Colon;
		}
		break;

		case Character_Period:
		{
			LexicalReturnValue = Period;

			Symbol += Character;
		}
		break;

		case Character_Semicolon:
		{
			LexicalReturnValue = SemiColon;

			Symbol += Character;
		}
		break;

		case Character_LeftParentheses:
		{
			LexicalReturnValue = LeftParentheses;

			Symbol += Character;
		}
		break;

		case Character_RightParentheses:
		{
			LexicalReturnValue = RightParentheses;

			Symbol += Character;
		}
		break;

		case Character_LeftBracket:
		{
			LexicalReturnValue = LeftBracket;

			Symbol += Character;
		}
		break;

		case Character_RightBracket:
		{
			LexicalReturnValue = RightBracket;

			Symbol += Character;
		}
		break;

		case Character_NullCharacter:
		{
			LexicalReturnValue = EndOfFile;
		}
		break;

		case Character_HorizontalTab:
		case Character_Space:
		{

		}
		break;
				
		default:
		{
			SCL_Helpers::Log("Unrecognized character.\nLine Number %i, Current Character Count %i", LineNumber, CurrentCharacterIndex);

			cout << "Unrecognized character." << endl;
			cout << "Line Number " << LineNumber << " Current Character Count " << CurrentCharacterIndex << endl;

			SetState(Error);
		}
		break;
	}

	return LexicalReturnValue;
}

void SCL_Scanner::AddEntry(Lexemes_and_Tokens_t &entry)
{

}

unsigned int SCL_Scanner::ScanLine(string& Line)
{
	bool FieldAccessor = false;
	bool FieldAccessorPointer = false;
	bool FoundDecimalAlready = false;
	bool SearchedKeywordTable = false;
	
	int ValueInLexicalEnum = Default;

	char Character = Character_NullCharacter;
	
	CurrentCharacterIndex = 0;
	LastCharacterIndex = 0;

	SetState(Normal);

	Lexemes_and_Tokens_t LexemesAndTokens;

	string line = Line;
	string Temp;

	do
	{
		FoundDecimalAlready = false;

		SearchedKeywordTable = false;

		ValueInLexicalEnum = Default;

		Symbol = "";

		if (GetCharacterFromLine(Line, Character))
		{
			if (InMultiLineComment == true)
			{
				if (GetCurrentCharacter() == Character_Asterisk && GetNextCharacter() == Character_BackSlash) // found */ change state now
				{
					InMultiLineComment = false;
				}
			}
			else
			{
				
				FieldAccessor = false;
				FieldAccessorPointer = false;
				
				if ((SCL_Helpers::IsAlphabetic(Character) || Character == Character_Underscore))
				{
					Symbol += Character;

					while (GetCharacterFromLine(Line, Character) &&
						(SCL_Helpers::IsAlphabeticOrNumeric(Character) || Character == Character_Underscore
							|| Character == Character_Period || Character == Character_Minus))
					{						
						if (Symbol.length() > MAX_BUFFER_SIZE)
						{
							SCL_Helpers::Log("Symbol too long.\nLine Number %i, Current Character Count %i", LineNumber, CurrentCharacterIndex);

							cout << "Symbol too long." << endl;
							cout << "Line Number " << LineNumber << " Current Character Count " << CurrentCharacterIndex << endl;
							
							return Error;
						}
						else // fix the issue with -> and . for member access, neccessary for interpreter to work!, recall that it looks for alpha, number, period, and minus.
						{
							if (Character == Character_Period)
							{
								FieldAccessor = true;

								ValueInLexicalEnum = Period;
							}
							else if (Character == Character_Minus)
							{
								if (GetNextCharacter() == Character_GreaterThan)
								{
									FieldAccessorPointer = true;

									ValueInLexicalEnum = Pointer;
								}
								else
								{
									break;
								}
							}
							else
							{
								Symbol += Character;
							}
						}
					}
					if (FieldAccessor != true && FieldAccessorPointer != true)
					{					
						map< unsigned int, unsigned int >::iterator SymbolMatchIterator = MapOfKeywords.find(SCL_Helpers::HashString(Symbol.c_str()));

						if (SymbolMatchIterator != MapOfKeywords.end())
						{
							ValueInLexicalEnum = SymbolMatchIterator->second;

							SearchedKeywordTable = true;
						}
						else
						{
							ValueInLexicalEnum = Identifier; // it is an identifier
						}
					}
					else
					{
						if (FieldAccessor == true)
						{
							Symbol += ".";
						}
						if (FieldAccessorPointer == true)
						{
							Symbol += "->";
						}
					}
				}
				else
				{					
					if (SCL_Helpers::IsDigit(Character))
					{
						ValueInLexicalEnum = IntegerConstant; // presume an integer until otherwise proven

						Symbol += Character;

						while (GetCharacterFromLine(Line, Character) && (SCL_Helpers::IsHexadecimalDigit(Character) ||
							Character == Character_Period || Character == Character_Minus || Character == Character_Plus
							|| Character == 'h') /*|| Character == 'x'*/)
						{
							Symbol += Character;

							if (Character == Character_Period)
							{
								if (FoundDecimalAlready == false)
								{
									ValueInLexicalEnum = FloatingPointConstant;

									FoundDecimalAlready = true;
								}
							}
							if (Character == 'h' /*|| Character == 'x'*/)
							{
								ValueInLexicalEnum = HexadecimalConstant;
							}
						}
					}
					else
					{
						ValueInLexicalEnum = CheckForNonSymbol(Line, Character);

						if (GetState() == Error)
						{
							return GetState();
						}
					}
				}
			}
		}
		if (Symbol.length() != 0 && ValueInLexicalEnum != Default)
		{
			LexemesAndTokens.TokenEntry.usekeywordtable = false; // explicit

			if (SearchedKeywordTable)
			{
				LexemesAndTokens.TokenEntry.usekeywordtable = true;
			}

			LexemesAndTokens.TokenEntry.token = ValueInLexicalEnum;
			LexemesAndTokens.LexemeEntry.lexeme = Symbol;

			LexemesAndTokens.Line = LineNumber;

			LexemesAndTokensVector.push_back(LexemesAndTokens);
		}
	}while(AtLineEnd(Line) != true);

	return GetState();
}

void SCL_Scanner::StartLexicalAnalysis()
{
	string Line;
	
	while (FileReader->ReadLine (Line))
	{
		if (ScanLine(Line) == Error)
		{
			break;
		}

		LineNumber++;
	}
}
#include "Python.h"
void SCL_Scanner::FinishLexicalAnalysis()
{
	string Lexeme;

	unsigned int Entry = 0, LastLine = LexemesAndTokensVector.front().Line;

	string LexemeFileName = "lexeme_";
	string TokenFileName = "token_";

	string::size_type Position = 0;

	LexemeFileName += FileReader->ReturnFileName();
	TokenFileName += FileReader->ReturnFileName();

	string SCL_Extension = ".scl";
			
	Position = LexemeFileName.find(SCL_Extension);

	if (Position != string::npos)
	{
		LexemeFileName.erase(Position);
	}

	Position = TokenFileName.find(SCL_Extension);

	if (Position != string::npos)
	{
		TokenFileName.erase(Position);
	}

	LexemeFileName += ".txt";
	TokenFileName += ".txt";

	if (GetState() != Error)
	{
		// log these to files
				
		ofstream FileLexeme;
		FileLexeme.open(LexemeFileName, ios::trunc);

		ofstream FileToken;
		FileToken.open(TokenFileName, ios::trunc);

		cout << "All tokens and lexemes scanned." << endl;
				
		for (unsigned int Iterator = 0; Iterator < LexemesAndTokensVector.size(); ++Iterator)
		{
			if (LastLine != LexemesAndTokensVector[Iterator].Line)
			{
				LastLine = LexemesAndTokensVector[Iterator].Line;

				cout << endl;
				FileLexeme << endl;
				FileToken << endl;
			}

			Entry = LexemesAndTokensVector[Iterator].TokenEntry.token;

			Lexeme = LexemesAndTokensVector[Iterator].LexemeEntry.lexeme;

			cout << "Lexeme: " << Lexeme << endl;
			FileLexeme << "Lexeme: " << Lexeme << endl;

			if (LexemesAndTokensVector[Iterator].TokenEntry.usekeywordtable == false)
			{
				cout << "Token: " << TokenNames[Entry] << " " << Entry << endl;
				FileToken << "Token: " << TokenNames[Entry] << " " << Entry << endl;
			}
			else
			{
				cout << "Token: " << "keyword" << " " << Entry << endl;
				FileToken << "Token: " << "keyword" << " " << Entry << endl;
			}
		}

		FileLexeme.close();
		FileToken.close();

		Py_Initialize();

		// Create some Python objects that will later be assigned values.

		PyObject *pName, *pModule, *pDict, *pFunc, *pArgs, *pValue1, *pValue2;

		// Convert the file name to a Python string.

		pName = PyString_FromString("main.py");

		// Import the file as a Python module.

		pModule = PyImport_Import(pName);

		// Create a dictionary for the contents of the module.

		pDict = PyModule_GetDict(pModule);

		// Get the add method from the dictionary.

		pFunc = PyDict_GetItemString(pDict, "main");

		// Create a Python tuple to hold the arguments to the method.

		pArgs = PyTuple_New(2);

		// Convert 2 to a Python integer.

		pValue1 = PyString_FromString(LexemeFileName.c_str());
		pValue2 = PyString_FromString(TokenFileName.c_str());

		// Set the Python int as the first and second arguments to the method.

		PyTuple_SetItem(pArgs, 0, pValue1);

		PyTuple_SetItem(pArgs, 1, pValue2);

		// Call the function with the arguments.

		PyObject* pResult = PyObject_CallObject(pFunc, pArgs);
		
		Py_Finalize();

//		system(shell.c_str());

		cout << endl << endl;

		cout << "Successfully read file. No errors." << endl;
		SCL_Helpers::Log("Successfully read file. No errors.");
	}
	else
	{
		cout << endl << endl;

		cout << "File had errors." << endl;
	}
}