/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 1st Deliverable

	CommonComponentInterface.h
	
	Purpose: Defines the interface class that derived classes use.
*/

class CommonComponentInterface
{
	public:
			
		virtual void ClassInitializationRoutine() = 0;			
		virtual void Start() = 0;
		virtual void Finish() = 0;
			
};