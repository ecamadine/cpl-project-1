/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 1st Deliverable

	CommonComponentInterface.h
	
	Purpose: Defines the required includes for this project, and any global defines necessary.
*/

#ifndef ISALREADYINCLUDED
#define ISALREADYINCLUDED 1

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <string.h>

#include <algorithm>
#include <list>
#include <vector>
#include <map>

#include <cstdarg>
#include <stdarg.h>
#include <ctime>


#define nullptr 0 // linux needs it explicitly defined

#if(MSC_VER)
#pragma warning(disable : 4996) //_CRT_SECURE_NO_WARNINGS
#endif

#define EscapeSequence_Apostrophe			'\''
#define EscapeSequence_BackSpace			'\b'
#define EscapeSequence_BackSlash			'\\'
#define EscapeSequence_CarriageReturn		'\r'
#define EscapeSequence_EscapeCharacter		'\e'
#define EscapeSequence_FormFeed				'\f'
#define EscapeSequence_HorizontalTab		'\t'
#define EscapeSequence_NewLine				'\n'
#define EscapeSequence_Quotation			'\"'
#define EscapeSequence_QuestionMark			'\?'
#define EscapeSequence_VerticalTab			'\v'

#define Character_Apostrophe				'\''
#define Character_Ampersand					'&'
#define Character_Asterisk					'*'
#define Character_At						'@'
#define Character_BackSpace					'\b'
#define Character_BackSlash					'/'
#define Character_CarriageReturn			'\r'
#define Character_Circumflex				'^'
#define Character_Colon						':'
#define Character_Comma						','
#define Character_DollarSign				'$'
#define Character_Equals					'='
#define Character_ExclamationMark			'!'
#define Character_FormFeed					'\f'
#define Character_ForwardSlash				'\\'
#define Character_GreaterThan				'>'
#define Character_LeftBracket				'['
#define Character_LeftCurlyBrace			'{'
#define Character_LeftParentheses			'('
#define Character_LessThan					'<'
#define Character_Modulus					'%'
#define Character_NewLine					'\n'
#define Character_NullCharacter				'\0'
#define Character_Minus						'-'
#define Character_Period					'.'
#define Character_Plus						'+'
#define Character_PoundSign					'#'
#define Character_RightBracket				']'
#define Character_RightCurlyBrace			'}'
#define Character_RightParentheses			')'
#define Character_QuestionMark				'?'
#define Character_Quotes					'\"'
#define Character_Semicolon					';'
#define Character_Space						' '
#define Character_HorizontalTab				'\t'
#define Character_Tilde						'~'
#define Character_Underscore				'_'
#define Character_VerticalBar				'|'
#define Character_VerticalTab				'\v'

#define MAX_BUFFER_SIZE 255

#endif