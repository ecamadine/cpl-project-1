/*
	University Name:	Kennesaw State University
	College:			College of Computing and Software Engineering
	Department:			Department of Computer Science
	Course:				CS 4308
	Course Title:		Concepts of Programming Languages
	Section:			Section W01
	Term:				Summer 2019
	Instructor:			Dr. Jose Garrido
	Student Name:		Eric Camadine
	Student Email:		ecamadin@students.kennesaw.edu
	Assignment:			Term Project 1st Deliverable
	
	main.cpp
	
	Purpose: Defines the entry point for this application.
*/

#include "RequiredHeaders.h"

using namespace std;

#include "SCL_FileReader.h"

#include "CommonComponentInterface.h"
#include "SCL_Helpers.h"
#include "SCL_Driver.h"

int main(int argc, char* argv[])
{
	int ExitCode = EXIT_SUCCESS;

	if (argc < 2)
	{
		cout << "Usage: SCL_Project sclfilename" << endl;

		ExitCode = EXIT_FAILURE;
	}
	else
	{
		SCL_Helpers::Log("");
		SCL_Helpers::Log("-------------------------------");
		SCL_Helpers::Log("-------------------------------");
		SCL_Helpers::Log("Logger initialized for file %s.", argv[1]);

		SCL_Driver Driver;

		Driver.Initialize(argv[1]);

		SCL_Helpers::Log("-------------------------------");
		SCL_Helpers::Log("-------------------------------");
		SCL_Helpers::Log("");
	}

	return ExitCode;
}